package com.inetbanking.pageObjects;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class AddNewCustomerPage {

	WebDriver ldriver;

	public AddNewCustomerPage(WebDriver rdriver) {

		ldriver = rdriver;

		PageFactory.initElements(ldriver, this);

	}
	
	@FindBy(how = How.XPATH, using ="/html/body/div[3]/div/ul/li[2]/a")
	@CacheLookup
	WebElement lnkAddNewCustomer;

	@FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[4]/td[2]/input")
	WebElement txtCustName;

	@FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[1]")
	WebElement radmaleGender;

	@FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[2]")
	WebElement radFemaleGender;

	@FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[7]/td[2]/textarea")
	WebElement txtAddress;

	@FindBy(xpath = "//*[@id=\"dob\"]")
	WebElement txtdob;

	@FindBy(name = "city")
	WebElement txtCity;

	@FindBy(name = "state")
	WebElement txtState;

	@FindBy(name = "pinno")
	WebElement txtPIN;

	@FindBy(xpath = "/html/body/table/tbody/tr/td/table/tbody/tr[7]/td[2]/textarea")
	WebElement txtDate;

	@FindBy(name = "telephoneno")
	WebElement txtTeleNum;

	@FindBy(name = "emailid")
	WebElement txtEmail;

	@FindBy(name = "sub")
	WebElement btnSubmit;

	@FindBy(name = "res")
	WebElement btnRest;

	@FindBy(xpath = "/html/body/p/a")
	WebElement linkHome;
	
	@FindBy(xpath="/html/body/div[3]/div/ul/li[2]/a")
	WebElement addNewCustomer;
	
	public void ClickAddNewCustomerLink() {

      //   ldriver.get("http://demo.guru99.com/V1/html/addcustomerpage.php");
	//	JavascriptExecutor js = (JavascriptExecutor) ldriver;
	//	js.executeScript("arguments[0].click();", addNewCustomer);
		lnkAddNewCustomer.click();

	}
	
	public void setCustName(String uname) {

		txtCustName.sendKeys(uname);

	}

	public void setAddress(String address) {

		txtAddress.sendKeys(address);

	}

	public void setCity(String city) {

		txtCity.sendKeys(city);

	}

	public void setState(String state) {

		txtState.sendKeys(state);

	}

	public void setEmail(String email) {

		txtEmail.sendKeys(email);

	}

	public void setTelNum(String telNum) {

		txtTeleNum.sendKeys(telNum);

	}

	public void setPin(String pin) {

		txtPIN.sendKeys(pin);

	}

	public void setDOB(String dob) {

		txtdob.sendKeys(dob);

	}

	public void setGenderMale() {
		radmaleGender.click();
	}

	public void setGenderFemale() {
		radFemaleGender.click();
	}

	public void clickSubmit() {

		btnSubmit.click();

	}
	
	
	

}
