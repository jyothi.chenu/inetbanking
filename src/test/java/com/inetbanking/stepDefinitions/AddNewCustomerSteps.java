package com.inetbanking.stepDefinitions;

import com.inetbanking.testCases.AddNewCustomerTest;
import com.inetbanking.testCases.BaseClass;
import com.inetbanking.testCases.LoginTest;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;


public class AddNewCustomerSteps extends BaseClass {
	
	@Given("^Open browser and login to the InetBanking login page$")
	public void assuming_user_already_logged_in() throws Throwable {
	
		super.setup();
		LoginTest loginTest = new LoginTest();
		loginTest.loginTest1();
	}


	@When("^enter all the required fields in new customer page$")
	public void enter_all_the_required_fields_in_add_new_customer_page() throws Throwable {
		AddNewCustomerTest addNewCustomerTest = new AddNewCustomerTest();
		addNewCustomerTest.addNewCustomerTest1();
	}

	@Then("^User search the new customer and valildates the new customer is added$")
	public void user_search_the_new_customer_and_valildates() throws Throwable {
	    System.out.println("search for the new customer");
	}

	@Then("^close the browser$")
	public void close_the_browser() throws Throwable {
	    
	  super.tearDown();  
	}


}
