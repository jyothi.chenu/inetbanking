package com.inetbanking.stepDefinitions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;

import com.inetbanking.pageObjects.LoginPage;
import com.inetbanking.testCases.AddNewCustomerTest;
import com.inetbanking.testCases.BaseClass;
import com.inetbanking.testCases.LoginTest;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps extends BaseClass {
	@Given("User Launch Chrome browser")
	public void user_launch_chrome_browser() throws Throwable {
		
		super.setup();

	}

	@When("User login to  InetBanking home page")
	public void user_login_to_inet_banking_home_page() {
		LoginTest loginTest = new LoginTest();  
	    loginTest.loginTest1();
	}

	@Then("User verify the title of the home page")
	public void user_verify_the_title_of_the_home_page() {
		LoginTest loginTest = new LoginTest();  
	    loginTest.loginHomePageTest1();

	}
	
	@Then("^User close the browser$")
	public void user_close_the_browser() throws Throwable {
	    super.tearDown();
	}


	
	
	

}
