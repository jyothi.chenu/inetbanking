package com.inetbanking.testRunner;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

//import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(plugin = { "pretty", "html:target/cucumber-report/cucumber.html" ,
		"json:target/cucumber-report/cucumber.json" }, features = "features", dryRun = false,glue="com/inetbanking/stepDefinitions")
public class CucumberRunner extends AbstractTestNGCucumberTests{
}
