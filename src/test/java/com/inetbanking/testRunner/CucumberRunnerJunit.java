package com.inetbanking.testRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber-report/cucumber.html" ,
"json:target/cucumber-report/cucumber.json" }, features = "features", dryRun = false,glue="com/inetbanking/stepDefinitions")
public class CucumberRunnerJunit  {
}
