package com.inetbanking.testCases;

import org.apache.log4j.Logger;

import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
//import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import inetBanking.utilities.ReadConfig;

public class BaseClass {
	
	ReadConfig readconfig = new ReadConfig();

	public String baseurl =readconfig.getApplicationURL();
	public String username =readconfig.getUsername();
	public String password = readconfig.getPassword();
	public static WebDriver driver;
	public static Logger logger;
	
	public String customername = readconfig.getCustomername();
	public String gender  = readconfig.getGender();
	public String dateofbirth = readconfig.getDateofbirth();
	public String address = readconfig.getAddress();
	public String city = readconfig.getCity();
	public String state = readconfig.getState();
	public String pin = readconfig.getPin();
	public String telenumber = readconfig.getTelenumber();
	public String email = readconfig.getEmail();
	
	public String homepageTitle = readconfig.getHomePageTitle();

	
	
	
	
	String chromePath = readconfig.getchromePath();
	String firefoxPath = readconfig.getfrefoxPath();
	String IEPath = readconfig.getIEPath();
	String browserTobeOpened = readconfig.browserTobeOpened();
	


	//@BeforeTest
	@BeforeSuite
	public void setup() throws InterruptedException 
	{
		if(browserTobeOpened.equals("chrome"))
		{
		System.setProperty("webdriver.chrome.driver",chromePath);
		driver = new ChromeDriver();
		}
		
		else if(browserTobeOpened.equals("fireFox"))
		{
		System.setProperty("webdriver.gecko.driver", firefoxPath);
		driver = new FirefoxDriver();
		}
		
		else if(browserTobeOpened.equals("IE"))
		{
		System.setProperty("webdriver.IE.driver", IEPath);
		driver = new InternetExplorerDriver();
		}
		
		
		
		logger  =Logger.getLogger("ebanking");
		PropertyConfigurator.configure("Log4j.properties");
		
		driver.get(baseurl);
		
		Thread.sleep(2000);
	}

	//@AfterTest
	@AfterSuite
	public void tearDown() {

		driver.quit();
	}

}
