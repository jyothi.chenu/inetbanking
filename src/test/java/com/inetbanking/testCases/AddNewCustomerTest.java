package com.inetbanking.testCases;

import org.testng.annotations.Test;

import com.inetbanking.pageObjects.AddNewCustomerPage;
import com.inetbanking.pageObjects.LoginPage;

public class AddNewCustomerTest extends BaseClass {

	@Test
	public void addNewCustomerTest1() throws InterruptedException {

		driver.manage().window().maximize();

		AddNewCustomerPage addObj = new AddNewCustomerPage(driver);

		addObj.ClickAddNewCustomerLink();

		addObj.setCustName(customername);
		if (gender.equals("male")) {
			addObj.setGenderMale();
		}

		if (gender.equals("female")) {
			addObj.setGenderFemale();
		}

		addObj.setDOB(dateofbirth);
		addObj.setAddress(address);
		addObj.setCity(city);
		addObj.setState(state);
		addObj.setPin(pin);
		addObj.setTelNum(telenumber);
		addObj.setEmail(email);
		Thread.sleep(2000);

		addObj.clickSubmit();

		System.out.println("entered all details");

	}

}
