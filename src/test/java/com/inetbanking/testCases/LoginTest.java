package com.inetbanking.testCases;

import org.testng.Assert;
import org.testng.annotations.Test;
//import org.testng.asserts.SoftAssert;

import com.inetbanking.pageObjects.LoginPage;

public class LoginTest extends BaseClass {

	@Test(priority = 1)
	public void loginTest1()

	{
		LoginPage lp = new LoginPage(driver);
		System.out.println("" + username);
		lp.setUsername(username);
		lp.setPassword(password);
		lp.clickSubmit();

	}

	@Test(priority = 2)
	public void loginHomePageTest1()

	{
		String currentTitle = driver.getTitle();
		Assert.assertEquals(currentTitle, homepageTitle);

	}

}
