package inetBanking.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

public class ReadConfig {

	Properties pro;

	public ReadConfig() {
		File src = new File("./Configuration/config.properties");
		try {
			FileInputStream fis = new FileInputStream(src);
			pro = new Properties();
			pro.load(fis);
		} catch (Exception e) {

			System.out.println("The exception is:" + e.getMessage());
		}

	}

	public String getApplicationURL() {
		String url = pro.getProperty("baseURL");
		return url;
	}

	public String getUsername() {
		String uname = pro.getProperty("username");
		return uname;
	}

	public String getPassword() {
		String pwd = pro.getProperty("password");
		return pwd;
	}

	public String getchromePath() {
		String chromePath = pro.getProperty("chromePath");
		return chromePath;
	}

	public String getfrefoxPath() {
		String frefoxPath = pro.getProperty("frefoxPath");
		return frefoxPath;
	}

	public String getIEPath() {
		String IEPath = pro.getProperty("IEPath");
		return IEPath;
	}

	public String browserTobeOpened() {
		String browserTobeOpened = pro.getProperty("browserTobeOpened");
		return browserTobeOpened;
	}

	public String getCustomername() {
		String customername = pro.getProperty("customername");
		return customername;
	}

	public String getGender() {
		String gender = pro.getProperty("gender");

		return gender;
	}

	public String getDateofbirth() {
		String dateofbirth = pro.getProperty("dateofbirth");

		return dateofbirth;
	}

	public String getAddress() {
		String address = pro.getProperty("address");

		return address;
	}

	public String getCity() {
		String city = pro.getProperty("city");

		return city;
	}

	public String getState() {
		String state = pro.getProperty("state");

		return state;
	}

	public String getPin() {
		String pin = pro.getProperty("pin");

		return pin;
	}

	public String getTelenumber() {
		String telenumber = pro.getProperty("telenumber");

		return telenumber;
	}

	public String getEmail() {
		String email = pro.getProperty("email");

		return email;
	}
	
	public String getHomePageTitle() {
		String homepageTitle = pro.getProperty("homepageTitle");
		return homepageTitle;
	}


}
